<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */

get_header(); ?>

    <div id="woods">
        <div class="main-woods-wrapper white container">
            <div id="blogs">
                <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                        echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } else {
                        echo 'no-thumbnail"';
                    } ?>>
                    <div class="container container-content">
                        <div class="content-set">
                            <?php if(is_year()) { ?>
                                <h1 class="title">
                                    <?php _e('Year ', 'leenderhof'); the_time('Y'); ?>
                                </h1> 
                            <?php } else if(is_month()) { ?>
                                <h1 class="title">
                                    <?php the_time('F Y'); ?>
                                </h1>
                            <?php } else if(is_date()) { ?>
                                <h1 class="title">
                                    <?php the_time('d F Y'); ?>
                                </h1>
                            <?php } else if(is_category()) { ?>
                                <h1 class="title">
                                    <?php single_term_title(); ?>
                                </h1>
                            <?php } ?>

                            <div class="subtitle">
                                <?php echo 'Showing ' . $GLOBALS['wp_query']->post_count . ' post'; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-list-post">
                    <div class="container">
                        <div class="row">
                            <?php if(have_posts()) :
                                while(have_posts()) : the_post();  ?>
                                <div class="column col-12 col-md-6 col-lg-4">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="item"<?php if( has_post_thumbnail() ){ echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                                            } ?>>
                                            <div class="overlay">
                                                <div class="wrapper">
                                                    <i class="icon far fa-image"></i>
                                                    <h2 class="title">
                                                        <?php the_title(); ?>
                                                    </h2>
                                                    <h6 class="subtitle">
                                                        <?php 
                                                            $catName = get_the_category();
                                                            if(!empty($catName)) : 
                                                                echo esc_html($catName[0]->name); 
                                                            endif; 
                                                        ?>
                                                    </h6>
                                                    <p class="content">
                                                        <?php echo content(20); ?>    
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="date">
                                            <?php the_time('F j, Y'); ?>
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile; 
                            endif; ?>

                        </div>
                    </div>
                </div>
                
                <?php get_template_part('template-parts/component/pagination'); ?>

                <?php get_template_part('template-parts/component/cta'); ?>
            </div>

<?php get_footer(); ?>
