jQuery(document).ready(function($){
    
    // Navbar, Toggle small navigation when user scroll
    function checkNavbar(){
        var scroll = $(window).scrollTop();
        if(scroll > 0){
            $("#the-navbar").addClass("small");
        }else{
            $("#the-navbar").removeClass("small");
        }
    }
    // Check onReady
    checkNavbar();
    // Check onScroll
    $(window).scroll(function(){
        checkNavbar();
    });

    // Prevent navbar toggling in transparent background
    $('.navbar-toggler').click(function(){
        var expanded = $(this).attr('aria-expanded');
        if( expanded != 'true' ){
            $('#the-navbar').addClass('navbar-expanded');
        } else {
            $('#the-navbar').removeClass('navbar-expanded');
        }
    });


    /** Swipebox */
    $( '.gallery-native,.wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox();
    });

    if($("#homepage").length){
        /** Menu Slider */
        $('.section-5 .slider').on('init', function(slick){
            $('.section-5').addClass('slider-loaded');
        }).slick({
            dots: true,
            appendDots: $(".section-5 .dots-wrapper"),
            arrows: false,
            autoplay: false,
            fade: true
        });
        $('.section-5 .dots-wrapper .slick-dots:nth-child(1)').addClass('active');

        $('.button-wrapper a').click(function(){
            var dataSlider = $(this).attr('data-slider');
            $('.section-5 .slider.active, .section-5 .dots-wrapper .slick-dots.active, .section-5 .button-wrapper a.active').removeClass('active');
            $('.section-5 .slider.data-slider-' + dataSlider +', .section-5 .dots-wrapper .slick-dots:nth-child('+ dataSlider +'), .section-5 .button-wrapper a:nth-child('+ dataSlider +')').addClass('active');
        });
    }

    /** Reserveren */
    if($('#reserveren').length){
        // Set Minimal date selection
        var thisDate = new Date(),
        thisMonth = thisDate.getMonth()+1,
        dataMonth = (thisMonth <= 9) ? '0' +thisMonth : thisMonth
        thisDay = thisDate.getDate(),
        dataDay = (thisDay <= 9) ? '0' + thisDay : thisDay;

        $("form input[type=\"date\"]").attr('min', thisDate.getFullYear() + "-" + dataMonth + "-" + dataDay);
    }
});