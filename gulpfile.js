// Disable SnoareToast notification
// process.env.DISABLE_NOTIFIER = true;

var gulp = require('gulp'),
	plumber = require('gulp-plumber'),
	autoprefixer = require('gulp-autoprefixer'),
	notify = require( 'gulp-notify' ),
	sass = require('gulp-sass'),
	include = require('gulp-include'),
	rename = require('gulp-rename'),
	uglify =  require('gulp-uglify-es').default,


	// Automatically reloads browser when files changed
	browserSync = require('browser-sync').create(),
	browserSyncOption = {
		files: [
			'./*.min.css',
			'./js/**/*.min.js',
			'./**/*.php',
			'./template-parts/**/*.php'
		],
		config: {
			watchTask: true,
			proxy: "http://localhost/"
		}
	},


	// All configuration
	config = {
		nodeDir: './node_modules',

		// Insert all the SASS Plugin HERE
		get sassPlugin(){
			return [
				// Bootstrap
				this.nodeDir + '/bootstrap/scss',
				// Font Awesome
				this.nodeDir + '/@fortawesome/fontawesome-free/scss',
				// Swipebox
				this.nodeDir + '/swipebox/scss',
				// Slick
				this.nodeDir + '/slick-carousel/slick',
				// JSsocials
				this.nodeDir + '/jssocials/styles'
			]
		}
	},


	// Options for compiler
	options = {

		// `Normal SASS` Compiler
		sass: {
			errLogToConsole: true,
			precision: 8,
			noCache: true,
			includePaths: config.sassPlugin
		},

		// `Minify SASS` Compiler
		sassmin: {
			errLogToConsole: true,
			precision: 8,
			noCache: true,
			outputStyle: "compressed",
			includePaths: config.sassPlugin
		}
	};


// Javascript
	// Compiler
	gulp.task('compile-js', function() {
		return gulp.src( './js/manifest.js' )
			.pipe( include() )
			// The normal version
			.pipe( rename( { basename: 'scripts' } ) )
			.pipe( gulp.dest( './js/dist' ) )
			// The minify version
			.pipe( uglify() )
			.pipe( rename( { suffix: '.min' } ) )
			.pipe( gulp.dest( './js/dist' ) )
			.pipe( notify({ message: 'scripts task complete' }));
	});
	// Watcher
	gulp.task('watch-js',function(){
		gulp.watch([ './js/**/*.js', '!./js/dist/*.js' ], gulp.series('compile-js'));
	});


// SASS
	// Compiler
	gulp.task('compile-sass', function () {

		return gulp.src('./sass/style.scss')
			// The normal version
			.pipe(plumber())
			.pipe(sass(options.sass))
			.pipe(autoprefixer())
			.pipe(gulp.dest('.'))
			// The minify version
			.pipe(sass(options.sassmin))
			.pipe(autoprefixer())
			.pipe( rename( { suffix: '.min' } ) )
			.pipe(gulp.dest('.'))
			.pipe(notify({ title: 'SASS', message: 'SASS have compiled !' }));
	});
	// Watcher
	gulp.task('watch-sass', function () {
		gulp.watch('./sass/**/*.scss', gulp.series('compile-sass'));
	});


// Stream files changes command
gulp.task('browser-sync', function(){
	browserSync.init(browserSyncOption.files, browserSyncOption.config);
	gulp.watch(browserSyncOption.files).on('change', function(){
		browserSync.reload();
	});
});


// `gulp watch` command
gulp.task('watch', gulp.parallel([
	gulp.series('watch-sass'),
	gulp.series('watch-js')
]));


// Default run of `gulp` command
gulp.task('default', gulp.parallel([
	gulp.series('compile-sass'),
	gulp.series('compile-js'),
	gulp.series('watch'),
	gulp.series('browser-sync')
]));