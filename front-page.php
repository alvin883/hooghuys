<?php
/**
 * @package jcm
 * @subpackage theme name here
 * 	
 */

get_header(); ?>
    <div id="woods">
        <div class="main-woods-wrapper container">

            <div id="homepage">
                <?php 
                    get_template_part('template-parts/homepage/section-1(hero)');
                    get_template_part('template-parts/homepage/section-2');
                    get_template_part('template-parts/homepage/section-3');
                    get_template_part('template-parts/homepage/section-4');
                    get_template_part('template-parts/homepage/section-5');
                    get_template_part('template-parts/homepage/section-6');
                    get_template_part('template-parts/homepage/section-7');
                ?>
            </div>

<?php 
get_footer();
