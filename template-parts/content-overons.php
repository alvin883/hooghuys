<?php
/**
 * Template part for displaying Overons Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Overons Page
 * @package wapps-theme
 */

 get_header(); ?>
 
    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php
                if(have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <div id="overons">
                            
                            <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                    echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                                } else {
                                    echo 'no-thumbnail"';
                                } ?>>
                                <div class="container container-content">
                                    <div class="content-set">
                                        <h1 class="title">
                                            <?php the_title(); ?>
                                        </h1>
                                        <?php if(get_field('subtitle_overons')) : ?>
                                            <div class="subtitle">
                                                <?php the_field('subtitle_overons'); ?>
                                            </div>
                                        <?php endif ; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="section section-the-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-8 mx-auto" id="the-content">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="section section-box-list">
                                <div class="container">
                                    <div class="content-set centered">
                                        <h2 class="title">
                                            <?php the_field('title_overons'); ?>
                                        </h2>
                                        <?php if(get_field('subtitle_overons')) : ?>
                                            <h6 class="subtitle">
                                                <?php the_field('subtitle2_overons'); ?>
                                            </h6>
                                        <?php endif; ?>
                                    </div>
                                    <div class="row">
                                        <?php if(have_rows('page_counter')) : 
                                            while(have_rows('page_counter')) : the_row();
                                                $the_object = get_sub_field('page_overons'); ?>
                                                <?php if($the_object) : 
                                                    $post = $the_object; setup_postdata($the_object); ?>
                                                    <div class="column col-12 col-md-6 col-lg-4 mx-auto">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <div class="item"
                                                                style="background-image: url('  <?php echo get_the_post_thumbnail_url(); ?> ');">
                                                                <div class="overlay">
                                                                    <div class="wrapper">
                                                                        <h3 class="title">
                                                                            <?php the_title(); ?>
                                                                        </h3>
                                                                        <h6 class="subtitle">
                                                                            <?php the_sub_field('content_highlight'); ?>
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php wp_reset_postdata(); ?>
                                                <?php endif ; ?>
                                                <!-- END DATA -->
                                            <?php endwhile ; 
                                        endif; ?>
                                                
                                    </div>
                                </div>
                            </div>

                            <?php get_template_part('template-parts/component/cta'); ?>

                        </div>
                <?php 
                    endwhile;
                endif;

get_footer();