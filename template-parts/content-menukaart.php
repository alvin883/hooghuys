<?php
/**
 * Template part for displaying Menukaart Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Menukaart Page
 * @package wapps-theme
 */

 get_header(); ?>

    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php 
            if(have_posts()) :
                while (have_posts()) : the_post(); ?>
                    <div id="impressie">
                        
                        <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                            } else {
                                echo 'no-thumbnail"';
                            } ?>>
                            <div class="container container-content">
                                <div class="content-set">
                                    <h1 class="title">
                                        <?php the_title(); ?>
                                    </h1>
                                    <?php if(get_field('subtitle_menukaart')) : ?>
                                        <div class="subtitle">
                                            <?php the_field('subtitle_menukaart'); ?>
                                        </div>
                                    <?php endif ; ?>
                                </div>
                            </div>
                        </div>
                        
                        <?php if(get_field('title_menukaart') && get_field('file_menukaart')) : ?>
                            <div class="section download">
                                <div class="container">
                                    <div class="content-set centered">
                                        <div class="title">
                                            <?php the_field('title_menukaart'); ?>
                                        </div>
                                        <div class="content">
                                            <?php the_field('content_menukaart'); ?>    
                                        </div>
                                        <a href="<?php $file = get_field('file_menukaart');
                                            if( $file ) : 
                                                echo $file['url'];
                                            endif;?>" class="btn red bigger"><?php the_field('text_button'); ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php get_template_part('template-parts/component/cta'); ?>

                    </div>
            <?php 
                endwhile;
            endif;

get_footer();