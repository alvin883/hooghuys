<?php
/**
 * Template part for displaying Impressie Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Impressie Page
 * @package wapps-theme
 */

 get_header(); ?>
 
    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php
                if(have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <div id="impressie">
                            
                            <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                    echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                                } else {
                                    echo 'no-thumbnail"';
                                } ?>>
                                <div class="container container-content">
                                    <div class="content-set">
                                        <h1 class="title">
                                            <?php the_title(); ?>
                                        </h1>
                                        <?php if(get_field('subtitle_impressie')) : ?>
                                            <div class="subtitle">
                                                <?php the_field('subtitle_impressie'); ?> 
                                            </div>
                                        <?php endif ; ?>
                                        <div class="content">
                                            <?php the_content(); ?>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="section section-full-gallery">
                                <div class="container">
                                    <?php
                                        if(get_field('gallery_section')) : 
                                            $image_ids = get_field('gallery_section', false, false);
                                            $shortcode = '[' . 'gallery ids="' . implode(',', $image_ids) . '"]'; 
                                            echo do_shortcode($shortcode);
                                        endif; 
                                    ?>
                                </div>
                            </div>

                            <?php get_template_part('template-parts/component/cta'); ?>

                        </div>
                <?php 
                    endwhile;
                endif;

get_footer();