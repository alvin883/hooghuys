<?php
/**
 * Template part for displaying Privacy Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Legals Page
 * @package wapps-theme
 */
get_header(); ?>

    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php
            if(have_posts()) :
            while (have_posts()) : the_post(); ?>
                    <div id="privacy">
                        
                        <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                            } else {
                                echo 'no-thumbnail"';
                            } ?>>
                            <div class="container container-content">
                                <div class="content-set">
                                    <h1 class="title">
                                        <?php the_title(); ?>
                                    </h1>
                                    <div class="subtitle">
                                        Last updated : <?php echo get_post_modified_time("d F Y", null, null, true); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="section section-the-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-8 mx-auto" id="the-content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php 
            endwhile;
            endif;

get_footer();