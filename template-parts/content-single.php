<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */

?>
	<div id="single">
		<div class="section section-1<?php 
				if( has_post_thumbnail() ){ 
					echo ' has-thumbnail" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
				}  else {
					echo ' " ';
				}
			?>>
			<?php if( !has_post_thumbnail() ) : ?>
				<div class="bg-jumbo-text">
					<?php the_title(); ?>
				</div>
			<?php endif; ?>
			<div class="container">
				<h2>
					<?php the_title(); ?>
				</h2>
				<h5>
					<?php the_time('F j, Y'); ?>
				</h5>
			</div>
		</div>

		<div class="section section-2">
			<div class="container">
				<div class="col-12 col-md-8 mx-auto">
					<div id="the-content">
						<?php the_content(); ?>
						<?php echo paginate_links();   ?>
					</div>
				</div>
			</div>
		</div>

		<div class="single-navigation">
			<div class="container">
				<?php
					$prevPost = get_adjacent_post( false, '', true );
					$nextPost = get_adjacent_post( false, '', false );
					
					if( $prevPost ) : ?>
						<a href="<?php the_permalink($prevPost->ID); ?>" class="left">
							<div>&laquo; Previous</div>
							<div><?php echo $prevPost->post_title; ?></div>
						</a>
					<?php endif; ?>
				
					<?php if( $nextPost ) : ?>
						<a href="<?php the_permalink($nextPost->ID); ?>" class="right">
							<div>Next &raquo;</div>
							<div><?php echo $nextPost->post_title; ?></div>
						</a>
					<?php endif; ?>
			</div>
		</div>
	</div>
