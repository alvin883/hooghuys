<?php
/**
 * Template part for displaying Contact Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Contact Page
 * @package wapps-theme
 */

 get_header(); ?>

    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php
                if(have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <div id="overons">
                            
                            <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                    echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                                } else {
                                    echo 'no-thumbnail"';
                                } ?>>
                                <div class="container container-content">
                                    <div class="content-set">
                                        <h1 class="title">
                                            <?php the_title(); ?>
                                        </h1>
                                        <?php if(get_field('subtitle_contact')) : ?>
                                            <div class="subtitle">
                                                <?php the_field('subtitle_contact'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="section section-the-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-8 mx-auto" id="the-content">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="section section-the-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-8 mx-auto" id="the-content">
                                            <?php echo do_shortcode(get_field('shortcode_contact')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <?php if(get_field('title_maps') && get_field('iframe_contact')) : ?>
                                <div class="section full-gmaps">
                                    <div class="container">
                                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                            <div class="content-set centered">
                                                <h2 class="title">
                                                    <?php the_field('title_maps'); ?>
                                                </h2>
                                                <div class="subtitle">
                                                    <?php the_field('subtitle_maps'); ?>
                                                </div>
                                                <div class="content">
                                                    <?php the_field('content_maps'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container maps">
                                        <div class="wrapper">
                                            <?php the_field('iframe_contact'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ; ?>

                        </div>
                <?php 
                    endwhile;
                endif;

get_footer();