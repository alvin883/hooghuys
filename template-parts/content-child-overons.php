<?php
/**
 * Template part for displaying Child Page of Overons
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Child Overons Page
 * @package wapps-theme
 */

 get_header(); ?>

    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php
                if(have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <div id="overons">
                            
                            <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                    echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                                } else {
                                    echo 'no-thumbnail"';
                                } ?>>
                                <div class="container container-content">
                                    <div class="content-set">
                                        <h1 class="title">
                                            <?php the_title(); ?>
                                        </h1>
                                        <?php if(get_field('subtitle_cpo')) : ?>
                                            <div class="subtitle">
                                                <?php the_field('subtitle_cpo'); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="section section-the-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-8 mx-auto" id="the-content">
                                            <p><?php the_content(); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <?php if(get_field('title_cpo') && get_field('content_cpo')) : ?>
                                <div class="section section-full-gallery">
                                    <div class="container">
                                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                            <div class="content-set centered">
                                                <h3 class="title">
                                                <?php the_field('title_cpo'); ?>
                                                </h3>
                                                <div class="subtitle">
                                                    <?php the_field('subtitle2_cpo'); ?>
                                                </div>
                                                <p class="content">
                                                    <?php the_field('content_cpo'); ?>
                                                </p>
                                            </div>
                                        </div>

                                        <!-- ACF Gallery -->
                                            <?php
                                            if(get_field('gallery_cpo')) : 
                                                $image_ids = get_field('gallery_cpo', false, false);
                                                $shortcode = '[' . 'gallery ids="' . implode(',', $image_ids) . '"]'; 
                                                echo do_shortcode($shortcode);
                                            endif; 
                                            ?>
                                        <!-- END ACF Gallery -->
                                        
                                    </div>
                                </div>
                            <?php endif ; ?>

                            <?php get_template_part('template-parts/component/cta'); ?>

                        </div>
                <?php 
                    endwhile;
                endif;

get_footer();