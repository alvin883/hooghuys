    <?php if(get_field('title_sec2', 'option') && get_field('content_sec2', 'option')) : ?>
        <div class="section section-2">
            <div class="container">
                <div class="row">
                    <?php $image_left = get_field('image_left', 'option'); 
                    if(!empty($image_left)) : ?> 
                        <div class="column col-4 photo left">
                            <div class="item"
                                style="background-image: url(' <?php echo $image_left['url']; ?> ');">
                            </div>
                        </div>
                    <?php endif ; ?>
                    <div class="column col-12 col-md-10 col-lg-4 mx-auto text">
                        <div class="item">
                            <div class="content-set centered">
                                <h2 class="title">
                                    <?php the_field('title_sec2', 'option'); ?>
                                </h2>
                                <h6 class="subtitle">
                                    <?php the_field('subtitle_sec2', 'option'); ?>
                                </h6>
                                <p class="content">
                                    <?php the_field('content_sec2', 'option'); ?>
                                </p>
                                <a href="<?php the_field('button_sec2', 'option'); ?>" class="btn outline red"><?php _e('lees meer', 'wapps-theme'); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php $image_right = get_field('image_right', 'option'); 
                    if(!empty($image_right)) : ?>
                        <div class="column col-4 photo right">
                            <div class="item"
                                style="background-image: url(' <?php echo $image_right['url']; ?> ');">
                            </div>
                        </div>
                    <?php endif ; ?>
                </div>
            </div>
        </div>
    <?php endif ; ?>