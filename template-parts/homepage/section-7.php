<div class="section section-7 section-list-post">
    <div class="container">
        <?php if(get_field('title_sec7', 'option') && get_field('subtitle_sec7', 'option') ) : ?>
            <div class="content-set centered">
                <div class="title">
                    <?php the_field('title_sec7', 'option'); ?>
                </div>
                <div class="subtitle">
                    <?php the_field('subtitle_sec7', 'option'); ?>
                </div>
            </div>
        <?php endif ; ?>

        <div class="row">
            <!-- start loo[p -->
            <?php   GLOBAL $post;
            $object = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 3,
            )); 
                if($object->have_posts()) :
                    while($object->have_posts()) : $object->the_post(); ?>
                        <div class="column col-12 col-md-6 col-lg-4">
                            <a href="<?php the_permalink(); ?>">
                                <div class="item"<?php if( has_post_thumbnail() ){ 
                                    echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"'; } ?>>
                                    <div class="overlay">
                                        <div class="wrapper">
                                            <i class="icon far fa-image"></i>
                                            <h2 class="title">
                                                <?php the_title(); ?>
                                            </h2>
                                            <h6 class="subtitle">
                                               <?php 
                                                    $name = get_the_category();
                                                    if(!empty($name)) {
                                                        echo esc_html($name[0]->name);
                                                    }
                                               ?>
                                            </h6>
                                            <div class="content">
                                                <?php the_field('highlight_content'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="date">
                                    <?php echo get_the_date(); ?>
                                </div>
                            </a>
                        </div>
                    <?php endwhile ; ?>
                <?php endif; ?>
            <!-- end loops -->
        </div>
    </div>
</div>