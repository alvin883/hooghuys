    <?php if(get_field('slider_item', 'option')) : ?>
        <div class="section section-5">
            <div class="container">
                <div class="content-set centered">
                    <h2 class="title">
                        <?php the_field('title_sec5', 'option'); ?>
                    </h2>
                    <h6 class="subtitle">
                        <?php the_field('subtitle_sec5', 'option'); ?>
                    </h6>
                </div>
                <?php if(have_rows('slider_item', 'option')) : ?> 
                    <div class="tab-slider">
                    <div class="button-wrapper">
                    <?php $i = 1; ?>
                        <?php while(have_rows('slider_item', 'option')) : the_row(); ?>
                            <a href='javascript:void(0);' class="btn grey <?php echo ($i==1) ? 'active' : ''; ?>" data-slider='<?php echo $i; ?>'><?php the_sub_field('category_name'); ?></a>
                            <?php  $i++; ?>
                        <?php endwhile ; ?> 
                        </div>
                            
                        <div class="slider-wrapper">
                        <?php $j = 1; ?>
                            <?php while(have_rows('slider_item', 'option')) : the_row(); ?>
                                <div class="<?php echo 'slider data-slider-'. $j . ' '. (($j==1) ? 'active' : ' '); ?>">
                                    <?php $j++; ?>
                                    <?php if(have_rows('item_breakdown')) : 
                                        while(have_rows('item_breakdown')) : the_row(); ?>
                                            <?php $image = get_sub_field('ground_image', 'option'); //var_dump($image);?>
                                                <div class="slider-item">
                                                    <div class="photo" style="background-image: url(' <?php echo $image['url']; ?> ');"></div>
                                                        <div class="box-content">
                                                            <div class="header">
                                                                <div class="title">
                                                                    <?php the_sub_field('title_list', 'option'); ?>
                                                                </div>
                                                                <div class="price">
                                                                    <?php the_sub_field('price_list', 'option'); ?>
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <?php the_sub_field('desc_list', 'option'); ?>
                                                            </div>
                                                        </div> 
                                                </div>
                                        <?php endwhile ; 
                                    endif ;  ?>
                                </div> <!-- slider --> 
                            <?php endwhile ;  ?>
                        </div><!--slider-wrapper -->
                    </div><!-- . tab-slider -->
                <?php endif; ?>
            
                <div class="bottom">
                    <div class="dots-wrapper"></div>
                    <a href="<?php the_field('refers_sec5', 'option'); ?>" class="to-menukaart btn outline red"><?php _e('bekijk menukaart', 'wapps-theme'); ?></a>
                </div>
            </div>
        </div>
    <?php endif ; ?>