<?php $image = get_field('image_sec6', 'option'); ?>
<?php if($image && get_field('video_sec6', 'option') && get_field('button_sec6', 'option')) : ?>
    <div class="section section-6">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <?php the_field('video_sec6', 'option'); ?>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="content-set centered">
                        <h2 class="title">
                            <?php the_field('title_sec6', 'option'); ?>
                        </h2>
                        <h6 class="subtitle">
                            <?php the_field('subtitle_sec6', 'option'); ?>
                        </h6>
                        <p class="content">
                            <?php the_field('content_sec6', 'option'); ?>
                        </p>
                        <a href=" <?php the_field('button_sec6', 'option'); ?>" class="btn outline red"><?php _e('impressie', 'wapps-theme'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>