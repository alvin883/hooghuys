
    <?php $image =  get_field('image_sec4', 'option'); ?>
    <?php if($image && get_field('button_sec4', 'option')) : ?>
        <div class="section section-4"
            style="background-image: url(' <?php echo $image['url']; ?> ');">
            <div class="container">
                <div class="content-set centered no-icon">
                    <h2 class="title">
                        <?php the_field('title_sec4', 'option'); ?>
                    </h2>
                    <p class="content">
                        <?php the_field('subtitle_sec4', 'option'); ?>
                    </p>
                    <a href="<?php the_field('button_sec4', 'option'); ?>" class="btn outline red"><?php _e('reserveren', 'wapps-theme');  ?></a>
                </div>
            </div>
        </div>
    <?php endif ; ?>