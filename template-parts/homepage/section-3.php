    <?php if(have_rows('service_card', 'option')) : ?>
        <div class="section section-3 section-box-list">
            <div class="container">
                <div class="row">
                    <?php if(have_rows('service_card', 'option')) :
                        while(have_rows('service_card', 'option')) : the_row();  ?>
                            <div class="column col-12 col-md-6 col-lg-4 mx-auto">
                                <a href="<?php the_sub_field('button_sec3', 'option'); ?>">
                                    <?php $image = get_sub_field('image_sec3', 'option'); ?>
                                    <div class="item"
                                        style="background-image: url(' <?php echo $image['url']; ?> ');">
                                        <div class="overlay">
                                            <div class="wrapper">
                                                <h3 class="title">
                                                    <?php the_sub_field('title_sec3', 'option'); ?>
                                                </h3>
                                                <h6 class="subtitle">
                                                    <?php the_sub_field('subtitle_sec3', 'option') ; ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile;
                    endif;  ?>
                </div>
            </div>
        </div>
    <?php endif; ?>