    <?php $image = get_field('image_sec1', 'option'); ?>
    <?php if(!empty($image)) : ?>
        <div class="section section-1" 
            style="background-image: url(' <?php echo $image['url']; ?>');">
            <?php if(get_field('title_sec1', 'option') && get_field('subtitle_sec1', 'option')) : ?>
                <div class="container">
                    <h1 class="title">
                        <?php the_field('title_sec1', 'option'); ?>
                    </h1>
                    <div class="icon"></div>
                    <h5 class="subtitle">
                        <?php the_field('subtitle_sec1', 'option'); ?>
                    </h5>
                    <a href="<?php the_field('button_sec1', 'option'); ?>" class="btn outline red"><?php _e('ontdek meer', 'wapps-theme'); ?></a>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>