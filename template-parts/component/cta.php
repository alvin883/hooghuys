
            
            <?php if(get_field('title_cta' ) && get_field('description_cta' )) : ?>
                <div class="section section-cta">
                    <div class="container">
                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                            <div class="content-set centered">
                                <h3 class="title">
                                    <?php the_field('title_cta' ); ?>
                                </h3>
                                <h6 class="subtitle">
                                    <?php the_field('subtitle_cta' ); ?>
                                </h6>
                                <div class="content">
                                    <?php the_field('description_cta' ); ?>
                                </div>
                                <div class="buttons">
                                    <?php if(get_field('page_btn1') && get_field('text_btn1')) : ?>
                                        <a href="<?php the_field('page_btn1' ); ?>" class="btn outline red">
                                            <?php the_field('text_btn1' ); ?>
                                        </a>
                                    <?php endif ; ?>

                                    <?php if(get_field('page_btn2') && get_field('text_btn2')) : ?>
                                        <a href="<?php the_field('page_btn2' ); ?>" class="btn red">
                                            <?php the_field('text_btn2' ); ?>
                                        </a>
                                    <?php endif ; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>