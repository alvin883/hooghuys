
        
        <!-- Bigger than 500 px screen -->
        <div class="post-navigation wide">
            <div class="info">
                <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                ?>
            </div>
            <div class="all-page-number <?php
                if( !paginate_links(array(
                    'total' => $wp_query->max_num_pages
                ))) echo 'no-pagination';
            ?>">
                <?php 
                    echo paginate_links(array(
                        'total' => $wp_query->max_num_pages
                    ));
                ?>
            </div>
        </div>

        <!-- Smaller than 500 px screen -->
        <div class="post-navigation mobile">
            <div class="info">
                <?php 
                    echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                ?>
            </div>
            <div class="all-page-number">
                <?php
                    previous_posts_link('&laquo; Previous');
                    next_posts_link('Next &raquo;'); 
                ?>
            </div>
        </div>