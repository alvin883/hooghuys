<?php
/**
 * Template part for displaying Reserveren Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Reserveren Page
 * @package wapps-theme
 */

 get_header(); ?>

    <div id="woods">
        <div class="main-woods-wrapper white container">
            <?php
            if(have_posts()) :
                while (have_posts()) : the_post(); ?>
                
                    <div id="reserveren">
                        
                        <div class="section full-thumbnail <?php if( has_post_thumbnail() ){ 
                                echo '" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                            } else {
                                echo 'no-thumbnail"';
                            } ?>>
                            <div class="container container-content">
                                <div class="content-set">
                                    <h1 class="title">
                                        <?php the_title(); ?>
                                    </h1>
                                    <?php if(get_field('subtitle_reserveren')) : ?>
                                        <div class="subtitle">
                                            <?php the_field('subtitle_reserveren'); ?>
                                        </div>
                                    <?php endif ; ?>
                                    <?php if(get_field('content_reserveren')) : ?>
                                        <div class="content">
                                            <?php the_field('content_reserveren'); ?>    
                                        </div>
                                    <?php endif ;?>
                                </div>
                            </div>
                        </div>

                        <div class="section section-the-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-8 mx-auto" id="the-content">
                                        <?php the_content(); ?>
                                        <?php echo do_shortcode(get_field('form_reserveren')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
            <?php 
                endwhile;
            endif;

get_footer();