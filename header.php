<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package leenderhof
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<nav class="navbar navbar-expand-xl fixed-top" id="the-navbar">
		<div class="container main-woods-wrapper navigation-bar">
			<div class="container the-navbar-container">
				<div class="left-side">
					<?php if (function_exists('the_custom_logo')) {
							the_custom_logo();
					}?>
				</div>
				
				<?php #only appear when screen <= `lg` size ?>
				<div class="right-side-collapse">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
				</div>

				<div class="right-side navbar-collapse collapse" id="navbarContent">
					<?php
						$args = array(
							'theme_location' => 'primary',
							'depth'      => 2,
							'container'  => false,
							'menu_class'     => 'navbar-nav',
							'walker'     => new Bootstrap_Walker_Nav_Menu()
							);
						if (has_nav_menu('primary')) {
							wp_nav_menu($args);
						}
					?>
				</div>

			</div>
		</div>
	</nav>