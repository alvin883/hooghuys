<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wapps-theme
 */

?>	


		<footer id="footer">

			<div class="top">
				<div class="container">
					<div class="row">

						<div class="main-column col-12 col-sm-6 col-lg-3 locatie">
							<div class="box">
								<div class="title">
									<?php _e('Onze Locatie', 'wapps-theme'); ?>
								</div>
								<div class="content">
									<div class="item">
										<div>
											<span>
												<?php _e('Hooghuys \'Breakfast, Lunch and drinks', 'wapps-theme');  ?>
											</span>
										</div>
										<?php the_field('adres', 'option'); ?>
									</div>
									<div class="item">
										<span>
											<?php _e('Bel om te reserveren:', 'wapps-theme'); ?>
										</span>
										<a href="tel:<?php the_field('telp', 'option'); ?>">
											<?php the_field('telp', 'option'); ?>
										</a>
										
										<br>
										
										<span>
											<?php _e('E-mail:', 'wapps-theme'); ?>
										</span>
										<a href="mailto:<?php the_field('email', 'option'); ?>">
											<?php the_field('email', 'option'); ?>
										</a>
									</div>

									<?php if(get_field('facebook', 'option') && get_field('instagram', 'option') && get_field('google', 'option') ) : ?>
										<div class="item socmed">
											<a href="<?php the_field('facebook', 'option'); ?>" target= "_blank" class="btn circle">
												<i class="fab fa-facebook-f"></i>
											</a>
											<a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="btn circle">
												<i class="fab fa-instagram"></i>
											</a>
											<a href="<?php the_field('google', 'option'); ?>" target="_blank" class="btn circle">
												<i class="fab fa-google-plus-g"></i>
											</a>
										</div>
									<?php endif ; ?>

								</div>
							</div>
						</div> 
						
						<div class="main-column col-12 col-sm-6 col-lg-3 news">
							<div class="box">
								<div class="title">
									<?php _e('laatste nieuws', 'wapps-theme'); ?>
								</div>
								<div class="content">
									<?php
										$child = new WP_Query(array(
											'post_type' => 'post',
											'posts_per_page' => 3,
										)); 
										if ($child->have_posts()) :
											while ($child->have_posts()) : $child->the_post(); ?>
												<a class="item" href="<?php the_permalink(); ?>">
													<div class="photo" <?php if( has_post_thumbnail() ){ 
															echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
														} ?>></div>
													<div class="data">
														<div class="title">
															<?php the_title(); ?>
														</div>
														<div class="text">
															<?php echo content(8);  ?>
														</div>
													</div>
												</a>
											<?php endwhile ; 
										endif ; ?> 
								</div>
							</div>	
						</div> 

						<div class="main-column col-12 col-sm-6 col-lg-3 opening">
							<div class="box">
								<div class="title">
									<?php _e('Openingstijden', 'wapps-theme'); ?>
								</div>
								<div class="content">
									<?php if (have_rows('daily_time', 'option')) :
										while (have_rows('daily_time', 'option')) : the_row(); ?>
											<div class="item">
												<div>
													<?php the_sub_field('days', 'option'); ?>
												</div>
												<div>
													<?php the_sub_field('time', 'option'); ?>
												</div>
											</div>
										<?php endwhile; 
									endif; ?>
								</div>
							</div>
						</div>

						<div class="main-column col-12 col-sm-6 col-lg-3 impressie">
							<div class="box">
								<div class="title">
									<?php _e('Impressie', 'wapps-theme'); ?>
								</div>
								<div class="content">
								<?php dynamic_sidebar('footer1'); ?>
								</div>
							</div>
						</div> 

						

					</div>
				</div>
			</div>

			<div class="bottom">
				<div class="logo">
					<?php if(function_exists('the_custom_logo')) {
						the_custom_logo(); 
					} ?>
				</div>
				<div class="text">
					<span class="red">&copy;</span> 
					2019 HOOGHUYS. - DESIGNED BY 
					<a class="red" href="https://www.wappstars.nl/" target="_blank">wappstars</a>
				<div>
			</div>
				
		</footer>

	</div> <?php # close #wrapper ?>
 </div> <?php # close #woods ?>

<?php 
	wp_footer();
?>
</body>
</html>

